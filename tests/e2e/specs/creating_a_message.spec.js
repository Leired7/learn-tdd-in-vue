describe('Creating a message', () => {
  it('Displays the message in the list', () => {
    cy.visit('/');
    // Visitar la página web

    cy.get("[data-test='messageText']").type('New message');
    // Escribir "New message" en el campo del mensaje

    cy.get("[data-test='sendButton']").click();
    // Clickar en un botón enviar

    cy.get("[data-test='messageText']").should('have.value', '');
    // Confirmar que el campo del mensaje tiene que estar vacío

    cy.contains('New message');
    // Confirmar que el mensaje "New message" que hemos escrito aparece en algún lugar de la pantalla
  });
});
